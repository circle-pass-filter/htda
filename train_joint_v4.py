import os
import matplotlib.pyplot as plt
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import torch.autograd as ta
from tqdm import tqdm
import torch
import numpy as np
import torch.optim as optim
from torchvision.utils import save_image
from collections import OrderedDict
import argparse

torch.backends.cudnn.benchmark = True
from tensorboardX import SummaryWriter
import seaborn as sns
from snn_feedback_model import *

parser = argparse.ArgumentParser()
parser.add_argument('--root_path', default='exp/large_model/cifar10_t10_c128_4', type=str)
parser.add_argument('--dataset', default='cifar10', type=str)
parser.add_argument('--gpu', default=2, type=int)
parser.add_argument('--num_classes', default=10, type=int)
parser.add_argument('--image_size', default=32, type=int)
parser.add_argument('--batch_size', default=200, type=int)
parser.add_argument('--time_steps', default=10, type=int)
parser.add_argument('--channel', default=128, type=int)
parser.add_argument('--lr', default=0.08, type=float)

parser.add_argument('--feedback_flag', default=True, type=bool)
parser.add_argument('--feedback_period', default=2, type=int)
parser.add_argument('--mask_loss_ratio', default=0.10, type=float)
parser.add_argument('--minimization_loss_ratio', default=0.40, type=float)
parser.add_argument('--smoothness_loss_ratio', default=0.09, type=float)
parser.add_argument('--T_max', default=6, type=float)
parser.add_argument('--sparsity_upper_bound', default=0., type=float)

parser.add_argument('--dynamic_learn', default=True, type=bool)
parser.add_argument('--v_th', default=0.30, type=float)
parser.add_argument('--decay', default=0.80, type=float)

args = parser.parse_args()

os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu)
lr = 0

os.system('rm -r {}'.format(args.root_path))
print('deleted: ', args.root_path)
os.mkdir(args.root_path)
print('new: ', args.root_path)

print('root path: ', args.root_path)
s_writer = SummaryWriter(args.root_path + '/runs')


transform_train = transforms.Compose([
    transforms.Resize(size=args.image_size),
    transforms.RandomHorizontalFlip(),
    transforms.RandomCrop(args.image_size, int(args.image_size / 8)),
    transforms.ToTensor(),
])

transform_test = transforms.Compose([
    transforms.Resize(size=args.image_size),
    transforms.ToTensor(),
])


if(args.dataset=='cifar10'):
    trainset = torchvision.datasets.CIFAR10(root='./data', train=True, download=True, transform=transform_train)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size, shuffle=True, num_workers=2)

    testset = torchvision.datasets.CIFAR10(root='./data', train=False, download=True, transform=transform_test)
    testloader = torch.utils.data.DataLoader(testset, batch_size=args.batch_size, shuffle=False, num_workers=2)

elif(args.dataset=='cifar100'):
    trainset = torchvision.datasets.CIFAR100(root='./data', train=True, download=True, transform=transform_train)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size, shuffle=True, num_workers=2)

    testset = torchvision.datasets.CIFAR100(root='./data', train=False, download=True, transform=transform_test)
    testloader = torch.utils.data.DataLoader(testset, batch_size=args.batch_size, shuffle=False, num_workers=2)

elif(args.dataset=='mnist'):
    trainset = torchvision.datasets.MNIST(root='./data', train=True, download=True, transform=transform_train)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size, shuffle=True, num_workers=2)

    testset = torchvision.datasets.MNIST(root='./data', train=False, download=True, transform=transform_test)
    testloader = torch.utils.data.DataLoader(testset, batch_size=args.batch_size, shuffle=False, num_workers=2)



net = NET(num_classes=args.num_classes, image_size=args.image_size, channel=args.channel, v_th=args.v_th,
          decay=args.decay,time_steps=args.time_steps).cuda()
criterion_cross_entropy = nn.CrossEntropyLoss().cuda()

std_para, dynamic_para = [], []
for k, v in net.named_parameters():
    if ('decay' in k or 'vth' in k):
        print(k)
        dynamic_para.append(v)
    else:
        std_para.append(v)

optimized_para = [{'params': std_para, 'weight_decay': 5e-4}]
if (args.dynamic_learn == True):
    optimized_para.append({'params': dynamic_para, 'weight_decay': 0})
optimizer = optim.SGD(optimized_para, lr=0.1, momentum=0.9)


def get_lr_list():
    base_lr = args.lr
    lr_list = []
    for i in range(10):
        lr_list.append((i + 1) * base_lr * 0.1)
    for i in range(40):
        lr_list.append(base_lr)
    for i in range(30):
        lr_list.append(base_lr * 0.1)
    for i in range(20):
        lr_list.append(base_lr * 0.01)
    return lr_list


lr_list = get_lr_list()


def mask_loss(mask_pred):
    if (mask_pred == []):
        return torch.zeros([1]).cuda()
    loss = 0
    for i in range(len(mask_pred)):
        mask_pred_temp = mask_pred[i]

        loss_temp1 = torch.relu(
            mask_pred_temp.view(args.batch_size, 1, -1).mean(dim=2) - args.sparsity_upper_bound).mean()
        loss_temp2 = abs_high_pass_filter(mask_pred_temp)
        loss_temp3 = -mask_pred_temp.mean(dim=1).view(-1, args.image_size ** 2).std(dim=1).mean()

        loss += loss_temp1 * args.minimization_loss_ratio \
                + loss_temp2 * args.smoothness_loss_ratio \
                + loss_temp3 * (1 - args.minimization_loss_ratio - args.smoothness_loss_ratio)

    return loss / len(mask_pred)


def train(epoch):
    net.train()

    lr = lr_list[epoch]
    for p in optimizer.param_groups:
        p['lr'] = lr

    T = 1 + epoch / 50 * (args.T_max - 1)
    if (T > args.T_max):
        T = args.T_max

    print('\nEpoch: %d,lr: %.5f,T:%.3f  ' % (epoch, lr, T), args.root_path)
    train_loss = 0
    train_loss_c = 0
    train_loss_m = 0

    correct = 0
    total = 0

    for batch_idx, (inputs, targets) in tqdm(enumerate(trainloader)):
        inputs, targets = inputs.cuda(), targets.cuda()


        if(inputs.size(1)==1):
            inputs=inputs.repeat(1,3,1,1)


        optimizer.zero_grad()

        classifier_pred, mask_pred = net(x=inputs, time_steps=args.time_steps, hidden=False, temperature=T,
                                         feedback_flag=args.feedback_flag, feedback_period=args.feedback_period)

        loss_m = mask_loss(mask_pred)
        loss_c = criterion_cross_entropy(classifier_pred, targets)

        loss = loss_c * (1 - args.mask_loss_ratio) + loss_m * args.mask_loss_ratio

        loss.backward()
        optimizer.step()

        train_loss += loss.item()
        train_loss_c += loss_c.item()
        train_loss_m += loss_m.item()

        _, predicted = classifier_pred.max(1)
        total += targets.size(0)
        correct += predicted.eq(targets).sum().item()

        indicator = int(len(trainloader) / 2)
        if ((batch_idx + 1) % indicator == 0):
            print(batch_idx, len(trainloader), 'Loss: %.3f ,loss_c:%.3f,loss_m:%.3f| Acc: %.3f%% (%d/%d)'
                  % (train_loss / (batch_idx + 1), train_loss_c / (batch_idx + 1), train_loss_m / (batch_idx + 1),
                     100. * correct / total, correct, total))


def test(epoch):
    net.eval()
    correct = 0
    total = 0

    T = 1 + epoch / 50 * (args.T_max - 1)
    if (T > args.T_max):
        T = args.T_max

    with torch.no_grad():
        for batch_idx, (inputs, targets) in tqdm(enumerate(testloader)):
            inputs, targets = inputs.cuda(), targets.cuda()

            if (inputs.size(1) == 1):
                inputs = inputs.repeat(1, 3, 1, 1)


            classifier_pred, mask_pred = net(x=inputs, time_steps=args.time_steps, hidden=False, temperature=T,
                                             feedback_flag=args.feedback_flag, feedback_period=args.feedback_period)

            total += targets.size(0)

            _, predicted = classifier_pred.max(1)
            correct += predicted.eq(targets).sum().item()

    print('test_acc:', 100. * correct / total)
    s_writer.add_scalar('accuracy', 100. * correct / total, epoch)

    torch.save(net.state_dict(), args.root_path + '/p_net_{}.pkl'.format(epoch))

    if (args.feedback_flag == True):

        for i in range(len(mask_pred)):
            s_writer.add_histogram('mask_{}'.format(i), mask_pred[i].detach().cpu().numpy().reshape(-1), epoch)

            pic_num = 200
            # masked_inputs = (inputs * mask_pred[-1][:, :3, :, :])[:pic_num, :, :, :]
            masked_inputs = (mask_pred[-1][:, :3, :, :])[:pic_num, :, :, :]
            result = torch.stack([inputs[:pic_num, :, :, :], masked_inputs], dim=0)
            result = result.permute(1, 0, 2, 3, 4).contiguous()
            result = result.view(-1, 3, args.image_size, args.image_size)
            save_image(result, args.root_path + '/temp.png', nrow=20)

            temp = plt.imread(args.root_path + '/temp.png').transpose(2, 0, 1)

            s_writer.add_image('masked_image_{}'.format(i), torch.FloatTensor(temp).cuda(), epoch)


def hidden_analysis(epoch):
    net.eval()
    with torch.no_grad():
        for batch_idx, (inputs, targets) in tqdm(enumerate(testloader)):
            inputs, targets = inputs.cuda(), targets.cuda()

            if (inputs.size(1) == 1):
                inputs = inputs.repeat(1, 3, 1, 1)

            break
        hidden = net(x=inputs, time_steps=args.time_steps, hidden=True, temperature=args.T_max,
                     feedback_flag=args.feedback_flag, feedback_period=args.feedback_period)

        w, h = len(hidden), len(hidden[0])
        matrix = np.zeros([w, h])
        for i in range(w):
            for j in range(h):
                matrix[i, j] = hidden[i][j].mean()
        sns.heatmap(matrix, annot=True)
        plt.savefig(args.root_path + '/heatmap_activation.png')
        plt.close()
        heatmap = plt.imread(args.root_path + '/heatmap_activation.png')[:, :, :3].transpose(2, 0, 1)
        s_writer.add_image('activation', torch.FloatTensor(heatmap).cuda(), epoch)


def print_args(args):
    dict = vars(args)
    print('arguments:--------------------------')
    args_file = open(args.root_path + '/args.txt', 'w')
    for key in dict.keys():
        line = '{}:{}'.format(key, dict[key])
        args_file.write(line + '\n')
        print(line)
    print('-----------------------------------')
    args_file.close()


def main():
    print_args(args)

    for i in range(100):
        train(epoch=i)
        test(epoch=i)
        if (i % 20 == 0):
            hidden_analysis(epoch=i)

main()
