import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

lens = 0.5

class ActFun(torch.autograd.Function):

    @staticmethod
    def forward(ctx, input):
        ctx.save_for_backward(input)
        return input.gt(0.).float()

    @staticmethod
    def backward(ctx, grad_output):
        input, = ctx.saved_tensors
        grad_input = grad_output.clone()
        temp = abs(input) < lens
        return grad_input * temp.float() / (lens * 2)


act_fun = ActFun.apply

def mem_update(operator, inputs, spike, mem, v_th, decay):
    decay = torch.clamp(decay, min=0, max=1)

    state = operator(inputs)
    mem = mem * (1 - spike) * decay + state
    temp = mem - v_th
    now_spike = act_fun(temp)
    # now_spike = nn.ReLU()(temp)
    return mem, now_spike.float()


class NET(nn.Module):
    def __init__(self, num_classes, image_size, channel, v_th, decay,time_steps):
        super(NET, self).__init__()

        self.cfg_cnn = [
            (3, channel, 1, 1, 3),
            (channel, channel, 1, 1, 3),

            (channel, channel * 2, 1, 1, 3),
            (channel * 2, channel * 2, 1, 1, 3),

            (channel * 2, channel * 4, 1, 1, 3),
            (channel * 4, channel * 4, 1, 1, 3),
        ]

        self.cfg_fc = [1024, 512, num_classes]
        self.cnn_dim = [image_size, image_size, int(image_size / 2), int(image_size / 2), int(image_size / 4),
                        int(image_size / 4)]
        self.fc_dim = int(self.cfg_cnn[-1][1] * self.cnn_dim[-1] * self.cnn_dim[-1] / 4)

        self.feedback_conv = nn.Sequential(
            (nn.ConvTranspose2d(in_channels=channel * 4, out_channels=channel * 2, kernel_size=4, stride=2, padding=1,
                                bias=True)),
            nn.ReLU(),
            (nn.ConvTranspose2d(in_channels=channel * 2, out_channels=channel, kernel_size=4, stride=2, padding=1,
                                bias=True)),
            nn.ReLU(),
            (nn.ConvTranspose2d(in_channels=channel, out_channels=1, kernel_size=4, stride=2, padding=1, bias=True)),
            nn.LayerNorm([image_size, image_size], elementwise_affine=False)
        )

        self.input_norm = nn.BatchNorm2d(num_features=3)

        in_planes, out_planes, stride, padding, kernel_size = self.cfg_cnn[0]
        self.conv1 = nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, stride=stride, padding=padding,
                               bias=True)

        in_planes, out_planes, stride, padding, kernel_size = self.cfg_cnn[1]
        self.conv2 = nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, stride=stride, padding=padding,
                               bias=True)

        in_planes, out_planes, stride, padding, kernel_size = self.cfg_cnn[2]
        self.conv3 = nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, stride=stride, padding=padding,
                               bias=True)

        in_planes, out_planes, stride, padding, kernel_size = self.cfg_cnn[3]
        self.conv4 = nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, stride=stride, padding=padding,
                               bias=True)

        in_planes, out_planes, stride, padding, kernel_size = self.cfg_cnn[4]
        self.conv5 = nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, stride=stride, padding=padding,
                               bias=True)

        in_planes, out_planes, stride, padding, kernel_size = self.cfg_cnn[5]
        self.conv6 = nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, stride=stride, padding=padding,
                               bias=True)

        self.fc1 = nn.Linear(self.fc_dim, self.cfg_fc[0])
        self.fc2 = nn.Linear(self.cfg_fc[0], self.cfg_fc[1])
        self.fc3 = nn.Linear(self.cfg_fc[1]*time_steps, self.cfg_fc[2])

        self.maxpool1 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.maxpool2 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.maxpool3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.decay_c1 = nn.Parameter(decay * torch.ones(1, self.cfg_cnn[0][1], 1, 1).cuda(), requires_grad=True)
        self.decay_c2 = nn.Parameter(decay * torch.ones(1, self.cfg_cnn[1][1], 1, 1).cuda(), requires_grad=True)
        self.decay_c3 = nn.Parameter(decay * torch.ones(1, self.cfg_cnn[2][1], 1, 1).cuda(), requires_grad=True)
        self.decay_c4 = nn.Parameter(decay * torch.ones(1, self.cfg_cnn[3][1], 1, 1).cuda(), requires_grad=True)
        self.decay_c5 = nn.Parameter(decay * torch.ones(1, self.cfg_cnn[4][1], 1, 1).cuda(), requires_grad=True)
        self.decay_c6 = nn.Parameter(decay * torch.ones(1, self.cfg_cnn[5][1], 1, 1).cuda(), requires_grad=True)
        self.decay_f1 = nn.Parameter(decay * torch.ones(1, self.cfg_fc[0]).cuda(), requires_grad=True)
        self.decay_f2 = nn.Parameter(decay * torch.ones(1, self.cfg_fc[1]).cuda(), requires_grad=True)

        self.register_parameter('decay_c1', self.decay_c1)
        self.register_parameter('decay_c2', self.decay_c2)
        self.register_parameter('decay_c3', self.decay_c3)
        self.register_parameter('decay_c4', self.decay_c4)
        self.register_parameter('decay_c5', self.decay_c5)
        self.register_parameter('decay_c6', self.decay_c6)
        self.register_parameter('decay_f1', self.decay_f1)
        self.register_parameter('decay_f2', self.decay_f2)

        self.vth_c1 = nn.Parameter(v_th * torch.ones(1, self.cfg_cnn[0][1], 1, 1).cuda(), requires_grad=True)
        self.vth_c2 = nn.Parameter(v_th * torch.ones(1, self.cfg_cnn[1][1], 1, 1).cuda(), requires_grad=True)
        self.vth_c3 = nn.Parameter(v_th * torch.ones(1, self.cfg_cnn[2][1], 1, 1).cuda(), requires_grad=True)
        self.vth_c4 = nn.Parameter(v_th * torch.ones(1, self.cfg_cnn[3][1], 1, 1).cuda(), requires_grad=True)
        self.vth_c5 = nn.Parameter(v_th * torch.ones(1, self.cfg_cnn[4][1], 1, 1).cuda(), requires_grad=True)
        self.vth_c6 = nn.Parameter(v_th * torch.ones(1, self.cfg_cnn[5][1], 1, 1).cuda(), requires_grad=True)
        self.vth_f1 = nn.Parameter(v_th * torch.ones(1, self.cfg_fc[0]).cuda(), requires_grad=True)
        self.vth_f2 = nn.Parameter(v_th * torch.ones(1, self.cfg_fc[1]).cuda(), requires_grad=True)

        self.register_parameter('vth_c1', self.vth_c1)
        self.register_parameter('vth_c2', self.vth_c2)
        self.register_parameter('vth_c3', self.vth_c3)
        self.register_parameter('vth_c4', self.vth_c4)
        self.register_parameter('vth_c5', self.vth_c5)
        self.register_parameter('vth_c6', self.vth_c6)
        self.register_parameter('vth_f1', self.vth_f1)
        self.register_parameter('vth_f2', self.vth_f2)

    def forward(self, x, time_steps=6, hidden=False, temperature=6, feedback_flag=False, feedback_period=2):

        conv1_spike, conv1_mem = 0, 0
        conv2_spike, conv2_mem = 0, 0
        conv3_spike, conv3_mem = 0, 0
        conv4_spike, conv4_mem = 0, 0
        conv5_spike, conv5_mem = 0, 0
        conv6_spike, conv6_mem = 0, 0

        fc1_spike, fc1_mem = 0, 0
        fc2_spike, fc2_mem = 0, 0

        feature_sum = 0
        attention_map = 1
        attention_map_list = []
        f2_spike_list=[]

        if (hidden == True):
            hidden_list = []

        for step in range(time_steps):
            y = x
            y = self.input_norm(y)

            if (feedback_flag == True):

                if (step == feedback_period):
                    feature_sum = feature_sum / feedback_period
                    attention_map = nn.Sigmoid()(self.feedback_conv(feature_sum).repeat(1, 3, 1, 1) * temperature)
                    attention_map_list.append(attention_map)
                    feature_sum = 0

                y = y * attention_map

            conv1_mem, conv1_spike = mem_update(self.conv1, y, conv1_spike, conv1_mem, self.vth_c1, self.decay_c1)
            conv2_mem, conv2_spike = mem_update(self.conv2, conv1_spike, conv2_spike, conv2_mem, self.vth_c2,
                                                self.decay_c2)

            y1 = self.maxpool1(conv2_spike)
            conv3_mem, conv3_spike = mem_update(self.conv3, y1, conv3_spike, conv3_mem, self.vth_c3, self.decay_c3)
            conv4_mem, conv4_spike = mem_update(self.conv4, conv3_spike, conv4_spike, conv4_mem, self.vth_c4,
                                                self.decay_c4)

            y2 = self.maxpool2(conv4_spike)
            conv5_mem, conv5_spike = mem_update(self.conv5, y2, conv5_spike, conv5_mem, self.vth_c5, self.decay_c5)
            conv6_mem, conv6_spike = mem_update(self.conv6, conv5_spike, conv6_spike, conv6_mem, self.vth_c6,
                                                self.decay_c6)

            y3 = self.maxpool3(conv6_spike)

            if (feedback_flag == True):
                feature_sum += y3

            linear_inputs = y3.view(-1, self.fc_dim)

            fc1_mem, fc1_spike = mem_update(self.fc1, linear_inputs, fc1_spike, fc1_mem, self.vth_f1, self.decay_f1)
            fc2_mem, fc2_spike = mem_update(self.fc2, fc1_spike, fc2_spike, fc2_mem, self.vth_f2, self.decay_f2)

            f2_spike_list.append(fc2_spike)

            if (hidden == True):
                hidden_list.append([conv1_spike.detach().cpu().numpy(),
                                    conv2_spike.detach().cpu().numpy(),
                                    conv3_spike.detach().cpu().numpy(),
                                    conv4_spike.detach().cpu().numpy(),
                                    conv5_spike.detach().cpu().numpy(),
                                    conv6_spike.detach().cpu().numpy(),
                                    fc1_spike.detach().cpu().numpy(),
                                    fc2_spike.detach().cpu().numpy(),
                                    ])

        outs=torch.cat(f2_spike_list,dim=1)
        outs = self.fc3(outs)

        if (hidden == False):
            return outs, attention_map_list
        else:
            return hidden_list


def abs_high_pass_filter(grad):
    kernel = np.array([[0, -1, 0], [-1, 4, -1], [0, -1, 0]])
    kernel = np.tile(kernel, (1, 3, 1, 1))
    kernel = torch.Tensor(kernel).cuda()
    bias = torch.zeros(kernel.size(0)).cuda()
    result = F.conv2d(grad, kernel, bias, 1, 0)
    result = torch.abs(result).mean()
    return result
