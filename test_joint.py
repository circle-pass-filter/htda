import os
import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.optim as optim
import torchvision.transforms as transforms
import torch.autograd as ta
from tqdm import tqdm
import torch
import torchvision
from collections import OrderedDict
from torchvision.utils import save_image
from snn_feedback_model import *
import numpy as np
import argparse

torch.backends.cudnn.benchmark = True

parser = argparse.ArgumentParser()
parser.add_argument('--root_path', default='exp/cifar10_feedback/t6_c128', type=str)
parser.add_argument('--dataset', default='cifar10', type=str)
parser.add_argument('--gpu', default=7, type=int)
parser.add_argument('--num_classes', default=10, type=int)
parser.add_argument('--image_size', default=32, type=int)
parser.add_argument('--batch_size', default=100, type=int)
parser.add_argument('--time_steps', default=6, type=int)
parser.add_argument('--channel', default=128, type=int)
parser.add_argument('--epoch', default=81, type=int)

parser.add_argument('--feedback_flag', action="store_true")
parser.add_argument('--feedback_period', default=2, type=int)
parser.add_argument('--T_max', default=6, type=float)

args = parser.parse_args()

os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu)

def load_state_dict(path):
    state_dict = torch.load(path)
    new_state_dcit = OrderedDict()
    for k, v in state_dict.items():
        if 'module' in k:
            name = k[7:]
        else:
            name = k
        new_state_dcit[name] = v
    return new_state_dcit


def get_real_data_set(dataset):
    transform_test = transforms.Compose([
        transforms.Resize(size=args.image_size),
        # transforms.RandomCrop(args.image_size, int(args.image_size / 8)+2),
        # transforms.RandomRotation(degrees=30,interpolation=transforms.InterpolationMode.BICUBIC),
        transforms.ToTensor(),
    ])

    if(dataset=='cifar10'):
        testset = torchvision.datasets.CIFAR10(root='./data', train=False, download=True, transform=transform_test)
        testloader = torch.utils.data.DataLoader(testset, batch_size=args.batch_size, shuffle=False, num_workers=4)
    elif(dataset=='cifar100'):
        testset = torchvision.datasets.CIFAR100(root='./data', train=False, download=True, transform=transform_test)
        testloader = torch.utils.data.DataLoader(testset, batch_size=args.batch_size, shuffle=True, num_workers=4)
    elif(dataset=='mnist'):
        testset = torchvision.datasets.MNIST(root='./data', train=False, download=True, transform=transform_test)
        testloader = torch.utils.data.DataLoader(testset, batch_size=args.batch_size, shuffle=True, num_workers=4)

    # testset = torchvision.datasets.ImageFolder(root='/data1/lfq1/tiny_imagenet/val',transform=transform_test)
    # testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size, shuffle=False, num_workers=2)

    return testloader


def get_net(path, epoch):
    net = NET(num_classes=args.num_classes, image_size=args.image_size, channel=args.channel, v_th=1000,
              decay=1000,time_steps=args.time_steps).cuda()
    net.eval()
    para = load_state_dict(path + '/p_net_{}.pkl'.format(epoch))
    net.load_state_dict(para)

    # net=nn.DataParallel(net,device_ids=range(5))

    return net

real_test_loader = get_real_data_set(args.dataset)
criterion_cross_entropy = nn.CrossEntropyLoss().cuda()
net = get_net(args.root_path, args.epoch)

def get_adv_inputs_pert(image, label, step_size, pert, iteration):
    delta = (torch.rand_like(image) - torch.rand_like(image)).cuda() * pert
    image = image.cuda()
    label = label.cuda()
    for i in range(iteration):
        temp = image + delta
        temp = temp.requires_grad_(True)
        c_pred, m_pred = net(x=temp, time_steps=args.time_steps, hidden=False, temperature=args.T_max,
                             feedback_flag=args.feedback_flag, feedback_period=args.feedback_period)
        outputs = c_pred
        loss = criterion_cross_entropy(outputs, label)
        grad = ta.grad(loss, temp, torch.ones_like(loss).cuda())[0]
        delta = delta + torch.sign(grad) * step_size
        delta[delta > pert] = pert
        delta[delta < -pert] = -pert
    image = image + delta
    image[image > 1] = 1
    image[image < 0] = 0
    return image


def save_images(adv_test, data_loader, pert,index):
    print('std rob test,pert:{}'.format(pert))

    for batch_idx, (inputs, targets) in (enumerate(data_loader)):
        if(batch_idx<index):
            continue
        inputs, targets = inputs.cuda(), targets.cuda()

        if (inputs.size(1) == 1):
            inputs = inputs.repeat(1, 3, 1, 1)

        if (adv_test == True):
            noise=torch.randn_like(inputs).cuda()*0.05
            inputs+=noise
            # inputs = get_adv_inputs_pert(inputs, targets, step_size=2 / 255, pert=pert / 255, iteration=10)

        c_pred, m_pred = net(x=inputs, time_steps=args.time_steps, hidden=False, temperature=args.T_max,
                             feedback_flag=args.feedback_flag, feedback_period=args.feedback_period)
        break

    if (args.feedback_flag == True):
        pic_num = 10

        mask= (m_pred[-1])[:pic_num, :, :, :]
        masked_inputs = (inputs * m_pred[-1])[:pic_num, :, :, :]
        result = torch.stack([inputs[:pic_num, :, :, :],mask, masked_inputs], dim=0)
        result = result.permute(1, 0, 2, 3, 4).contiguous()
        result = result.view(-1, 3, 32, 32)
        save_image(result, args.root_path + '/masked_{}.png'.format(index), nrow=9)


for i in range(1,100):
    save_images(adv_test=False, data_loader=real_test_loader, pert=8 / 255, index=i)
print('done')



def save_map_hist(data_loader):
    mask_list=[]
    for batch_idx, (inputs, targets) in (enumerate(data_loader)):

        inputs, targets = inputs.cuda(), targets.cuda()

        if (inputs.size(1) == 1):
            inputs = inputs.repeat(1, 3, 1, 1)

        c_pred, m_pred = net(x=inputs, time_steps=args.time_steps, hidden=False, temperature=args.T_max,
                             feedback_flag=args.feedback_flag, feedback_period=args.feedback_period)
        mask_list.append(m_pred[0].detach().cpu().numpy())
    mask_list=np.concatenate(mask_list,axis=0)
    plt.hist(mask_list.reshape(-1),bins=100)
    plt.xticks(fontsize=19)
    plt.yticks(fontsize=19)
    plt.savefig(args.root_path+'/hist.png',dpi=1000)

# save_map_hist(real_test_loader)


def scan_accuracy(data_loader):
    acc_list=[]
    i_list=[]

    for i in range(80,100):
        net=get_net(args.root_path,i)
        correct = 0
        total = 0
        for batch_idx, (inputs, targets) in (enumerate(data_loader)):
            inputs, targets = inputs.cuda(), targets.cuda()

            if (inputs.size(1) == 1):
                inputs = inputs.repeat(1, 3, 1, 1)

            c_pred, m_pred = net(x=inputs, time_steps=args.time_steps, hidden=False, temperature=args.T_max,
                                 feedback_flag=args.feedback_flag, feedback_period=args.feedback_period)

            _, predicted = c_pred.max(1)
            total += targets.size(0)
            correct += predicted.eq(targets).sum().item()
        acc=100. * correct / total
        acc_list.append(acc)
        i_list.append(i)
        print(i, acc)
    print('max:-------',np.max(acc_list))

# scan_accuracy(real_test_loader)


def std_rob_test(data_loader):
    pert_list=[0,1,2,3,4,5,6]
    acc_list=[]
    for pert in pert_list:
        step_size = 2
        if(args.dataset=='mnist'):
            pert*=10
            step_size*=10
        correct = 0
        total = 0
        for batch_idx, (inputs, targets) in (enumerate(data_loader)):
            inputs, targets = inputs.cuda(), targets.cuda()

            if (inputs.size(1) == 1):
                inputs = inputs.repeat(1, 3, 1, 1)

            # noise=torch.randn_like(inputs).cuda()*0.05*pert
            # inputs+=noise
            inputs = get_adv_inputs_pert(inputs, targets, step_size=step_size / 255, pert=pert / 255, iteration=10)

            c_pred, m_pred = net(x=inputs, time_steps=args.time_steps, hidden=False, temperature=args.T_max,
                                 feedback_flag=args.feedback_flag, feedback_period=args.feedback_period)

            _, predicted = c_pred.max(1)
            total += targets.size(0)
            correct += predicted.eq(targets).sum().item()
        acc=correct/total*100
        acc_list.append(acc)
        print(args.root_path,pert,acc)
    np.save(args.root_path+'/adv_acc.npy',acc_list)


# std_rob_test(real_test_loader)


def counter_spike(data_loader):
    ratio_list = []
    for batch_idx, (inputs, targets) in (enumerate(data_loader)):
        inputs, targets = inputs.cuda(), targets.cuda()
        if (inputs.size(1) == 1):
            inputs = inputs.repeat(1, 3, 1, 1)

        spike_sum = 0
        totoal_sum = 0
        hidden_list = net(x=inputs, time_steps=args.time_steps, hidden=True, temperature=args.T_max,
                          feedback_flag=args.feedback_flag, feedback_period=args.feedback_period)

        hidden_list=hidden_list[args.feedback_period:]

        for item in hidden_list:
            for jtem in item:
                spike_sum += jtem.sum()
                totoal_sum += jtem.size

        ratio = spike_sum / totoal_sum
        ratio_list.append(ratio)
    print(args.root_path, np.mean(ratio_list))
    np.save(args.root_path + '/fire_rate.npy', np.mean(ratio_list))


def save_masked_hidden(data_loader, index):

    for batch_idx, (inputs, targets) in (enumerate(data_loader)):
        if(batch_idx<index):
            continue
        inputs, targets = inputs.cuda(), targets.cuda()

        hidden_list = net(x=inputs, time_steps=args.time_steps, hidden=True, temperature=args.T_max,
                             feedback_flag=args.feedback_flag, feedback_period=args.feedback_period)
        print(len(hidden_list))
        break

    inputs=inputs.detach().cpu().numpy().transpose(0,2,3,1)[index]
    plt.imsave('spike_image.png',inputs)
    input()

    for image_index in range(100):
        print(image_index)
        image_list = []
        for i in range(args.time_steps):
            image_list.append(hidden_list[i][0][image_index].mean(axis=0))
        result = np.concatenate(image_list, axis=1)
        np.save('spike_show_b/image_{}.npy'.format(image_index),result)

    # if (args.feedback_flag == True):
    #     pic_num = 12
    #
    #     mask= (m_pred[-1])[:pic_num, :, :, :]
    #     masked_inputs = (inputs * m_pred[-1])[:pic_num, :, :, :]
    #     result = torch.stack([inputs[:pic_num, :, :, :],mask, masked_inputs], dim=0)
    #     result = result.permute(1, 0, 2, 3, 4).contiguous()
    #     result = result.view(-1, 3, 32, 32)
    #     save_image(result, args.root_path + '/masked_{}.png'.format(index), nrow=9)

# save_masked_hidden(real_test_loader,1)

# counter_spike(data_loader=real_test_loader)


#
# def std_rob_testss(adv_test, data_loader, pert):
#     print('std rob test,pert:{}'.format(pert))
#     test_loss = 0
#     correct = 0
#     total = 0
#     matrix = np.zeros((10, 10))
#
#     for batch_idx, (inputs, targets) in (enumerate(data_loader)):
#         inputs, targets = inputs.cuda(), targets.cuda()
#
#         if (inputs.size(1) == 1):
#             inputs = inputs.repeat(1, 3, 1, 1)
#
#
#         if (adv_test == True):
#             # noise=torch.randn_like(inputs).cuda()*0.05*pert
#             # inputs+=noise
#
#             inputs = get_adv_inputs_pert(inputs, targets, step_size=2 / 255, pert=pert / 255, iteration=10)
#
#         c_pred, m_pred = net(x=inputs, time_steps=args.time_steps, hidden=False, temperature=args.T_max,
#                              feedback_flag=args.feedback_flag, feedback_period=args.feedback_period)
#
#         loss = criterion_cross_entropy(c_pred, targets)
#         test_loss += loss.item()
#         _, predicted = c_pred.max(1)
#         total += targets.size(0)
#         correct += predicted.eq(targets).sum().item()
#
#         for i in range(inputs.size(0)):
#             matrix[predicted[i], targets[i]] += 1
#
#     print('Loss: %.3f |test Acc: %.3f%% (%d/%d),adv:%d' % (
#         test_loss / (batch_idx + 1), 100. * correct / total, correct, total, adv_test))
#
#     if (args.feedback_flag == True):
#         pic_num = 200
#         # masked_inputs = (inputs * m_pred[-1])[:pic_num, :, :, :]
#         masked_inputs = (m_pred[-1])[:pic_num, :, :, :]
#         result = torch.stack([inputs[:pic_num, :, :, :], masked_inputs], dim=0)
#         result = result.permute(1, 0, 2, 3, 4).contiguous()
#         result = result.view(-1, 3, 32, 32)
#         save_image(result, args.root_path + '/test_adv{}_pert{}.png'.format(adv_test, pert), nrow=20)
#
#     return correct / total
#
#
# def mask_rob_test(adv_test, data_loader, pert, radius):
#     print('mask rob test,pert:{},radius:{}'.format(pert, radius))
#     test_loss = 0
#     correct = 0
#     total = 0
#     matrix = np.zeros((10, 10))
#
#     noise_mask = torch.zeros(32, 32)
#     for i in range(32):
#         for j in range(32):
#             if (np.sqrt((i - 16) ** 2 + (j - 16) ** 2) > radius):
#                 noise_mask[i, j] = 1
#     noise_mask = noise_mask.repeat(args.batch_size, 3, 1, 1).cuda()
#
#     for batch_idx, (inputs, targets) in (enumerate(data_loader)):
#         inputs, targets = inputs.cuda(), targets.cuda()
#         if (inputs.size(1) == 1):
#             inputs = inputs.repeat(1, 3, 1, 1)
#
#         if (adv_test == True):
#             noise = torch.randn_like(inputs).cuda() * 0.05 * pert * noise_mask
#             inputs += noise
#
#             # inputs = get_adv_inputs_pert(inputs, targets, step_size=2 / 255, pert=pert / 255, iteration=10)
#
#         c_pred, m_pred = net(x=inputs, time_steps=args.time_steps, hidden=False, temperature=args.T_max,
#                              feedback_flag=args.feedback_flag, feedback_period=args.feedback_period)
#
#         loss = criterion_cross_entropy(c_pred, targets)
#         test_loss += loss.item()
#         _, predicted = c_pred.max(1)
#         total += targets.size(0)
#         correct += predicted.eq(targets).sum().item()
#
#         for i in range(inputs.size(0)):
#             matrix[predicted[i], targets[i]] += 1
#
#     print('Loss: %.3f |test Acc: %.3f%% (%d/%d),adv:%d' % (
#         test_loss / (batch_idx + 1), 100. * correct / total, correct, total, adv_test))
#
#     if (args.feedback_flag == True):
#         pic_num = 200
#         # masked_inputs = (inputs * m_pred[-1])[:pic_num, :, :, :]
#         masked_inputs = (m_pred[-1])[:pic_num, :, :, :]
#         result = torch.stack([inputs[:pic_num, :, :, :], masked_inputs], dim=0)
#         result = result.permute(1, 0, 2, 3, 4).contiguous()
#         result = result.view(-1, 3, 32, 32)
#         save_image(result, args.root_path + '/test_adv{}_pert{}.png'.format(adv_test, pert), nrow=20)
#
#     return correct / total, matrix
#
